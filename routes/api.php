<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//no token authentication at the moment
Route::middleware('auth:api')->get('/list','TaskController@list');
Route::middleware('auth:api')->get('/past_hour_stats','StatsController@getPastHourStats');
Route::middleware('auth:api')->post('/complete_task','TaskController@completeTask');