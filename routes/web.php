<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::resource('tasks','TaskController')->middleware('auth');

//have no home page so redirecting to tasks page 
Route::get('/', function(){
    return redirect('/tasks');
})->name('home');

//default auth controllers were redirecting back to '/home' added this redirect rule to avoid errors
Route::get('/home',function(){
    return redirect('/');
});