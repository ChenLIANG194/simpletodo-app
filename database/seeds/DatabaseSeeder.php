<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        if (!User::where('name','admin')->count()){
            DB::table('users')->insert([
                'name' => 'admin',
                'email' => 'admin@test.com',
                'password' => Hash::make("test1234"),
                'api_token' => Str::random(60)
            ]);
        }
        
        $count = (int)$this->command->ask('How many task do you need for users?', 50);

        $this->command->info("Creating {$count} Tasks.");

        factory(App\Tasks::class,$count)->states('random','withActualUser')->create();
    }
}
