<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Tasks;
use App\User;
use Faker\Generator as Faker;

$factory->define(Tasks::class, function (Faker $faker) {
    $event = $faker->dateTimeBetween('-2 hours','now'); 
    return [
        'user_id' => 1,
        'description' => $faker->text,
        'is_completed' => false,
        'created_at'=>$event,
        'updated_at'=>$event
    ];
});

$factory->state(Tasks::class,'random', function (Faker $faker) {
    $event = $faker->dateTimeBetween('-2 hours','now');
    return [
        'is_completed' => $faker->boolean($chanceOfGettingTrue = 30),
        'created_at'=>$event,
        'updated_at'=>$event->add(new DateInterval('PT'.$faker->numberBetween(1,60).'M'))
    ];
});

$factory->state(Tasks::class,'withActualUser', function (Faker $faker) {
    $users = User::all()->pluck('id')->toArray();
    return [
        'user_id' => $faker->randomElement($users),
    ];
});
