<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * relational definition with Tasks Model
     * hasMany tasks
     *
     * @return Illuminate\Database\Eloquent\Relations\Relation;
     */
    public function tasks()
    {
        return $this->hasMany(Tasks::class);
    }

    public function generateNewToken()
    {
        $this->api_token = Str::random(60);
        $this->save();
    }

    /**
     * adds The task instance to user then save the task
     *
     * @param string $description
     * @return void
     */
    public function addTask($description)
    {
        try {
            return Tasks::create([
                'user_id' => $this->id,
                'description' => $description
            ]);
        } catch (\Throwable $e) {
            throw $e;
        }
    }
}
