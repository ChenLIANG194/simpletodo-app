<?php

namespace App;

use App\Tasks;
use Carbon\Carbon;

/**
 * Model for functions related to generating stats based on tasks
 */
class Stats
{

    /**
     * get stats array for the past hour
     *
     * @return Array
     */
    public static function getStatsForPastHour()
    {

        $task_collection = auth()->user()->tasks;
        //generate result array based on every mins for the past hour
        $stats = self::generateStatsData(
            //passing in a anonymous function as callback to get the  
            function ($time) use ($task_collection) {
                //get number of completed task that has been updated before the $time
                $pending_count_by_min = $task_collection->where('created_at', '<=', $time)->count();
                $completed_count_by_update = $task_collection->where('is_completed', true)->where('updated_at', '<=', $time)->count();
                return $pending_count_by_min-$completed_count_by_update;
            }
        );

        return ['labels'=>array_keys($stats), 'data'=>array_values($stats)];
    }

    /**
     * generate the stats array required
     * 
     * an associative array with 'HourMins' as key and number of tasks remaining as value 
     *
     * @param callable $callback
     * @return Array
     */
    public static function generateStatsData(callable $callback)
    {
        // initializing minits array
        $mins_array = [];

        $time = Carbon::now()->subMinutes(60);

        // loop 60 times, everytime adds a min to $time to get every min of the past hour
        for ($i = 0; $i < 60; $i++) {
            $time->addMinute();
            $min_text = $time->format("Hi");
            $mins_array[$min_text] = call_user_func($callback, $time);
        }
        return $mins_array;
    }
}
