<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tasks extends Model
{
    // defines mass fillable attributes
    protected $fillable = ['user_id','description', 'is_completed'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function markCompleted()
    {
        $this->is_completed = true;
        // $this->complete_at=
        $this->save();
    }
}
