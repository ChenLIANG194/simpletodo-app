<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tasks;
use App\Http\Resources\Tasks as TasksResource;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Tasks::all()->first()->updated_at->timestamp);
        return view('tasks.index');
    }
    
    /**
     * return list of Task for API
     *
     * @return collection
     */
    public function list()
    {
        return TasksResource::collection(auth()->user()->tasks->sortByDesc('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate([
            'description'=>'required|min:3'
        ]);
        
        auth()->user()->addTask(request('description'));
        
        return redirect('/tasks');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    /**
     * Mark a task Completed
     *
     * @param $id
     * @return void
     */
    public function completeTask()
    {
        $task = Tasks::find(request('task_id'));
        $task->is_completed =true;
        $task->save();
        return;
    }

}
