<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \App\User;
use \Faker\Factory as Faker;

class UserTest extends TestCase
{
    use WithFaker,RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testUserCreate()
    {
        
        $user = factory(User::class)->create();

        $this->assertNotNull($user->id);
    }

    /**
     * test add Task method for user
     *
     * @return void
     */
    public function testUserCanAddTasks()
    {
        $user = factory(User::class)->create();
        $faker = Faker::create();
        $task = $user->addTask($faker->text);
        $this->assertNotEmpty($task);
        $this->assertEquals($user->id,$task->user_id);

    }
}
