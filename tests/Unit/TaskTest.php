<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Tasks;

class TaskTest extends TestCase
{
    use withFaker,RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * tests task's complete task method
     *
     * @return void
     */
    public function testCompleteTask()
    {
        $task = factory(Tasks::class)->create();
        $this->assertFalse($task->is_completed);
        
        $task->markCompleted();
        $this->assertTrue($task->is_completed);
    }
}
