# Simple Todo List
Simple Todo List, allow user register, login, create tasks, will show list of pending task, completed task as well as a burndown chart of pending tasks in the past hour
## Installation Steps
**Clone the repo**
```
git clone git@bitbucket.org:ChenLIANG194/simpletodo-app.git
```
For this demo, .env is included for simplicities sake, normally you will need to create your own .env

**Run docker-compose**
```
docker-compose up
```
Note: I have to change the EOL of docker-entrypoint.sh on some windows computer, otherwise it would give me "no such file or directory" error.

**Run migration**
```
docker-compose exec app php artisan migrate
```

#### you can choose to run seeding, you will be creating an admin account with credentials as follows if you do not have a account with name admin:
```
docker-compose exec app php artisan db:seed

admin@test.com
test1234
```
### afterwards you will be prompted to input number of tasks you would like to create, default 50

### Finaly the port is mapped to 8810 (localhost:8810), feel free to change to another port if its already assigned.

## PHPUnit Test
To run the unit test, go to the project root and run
runing unit test will run migration, need to run seed afterwards to populate data.
```
docker-compose exec app ./vendor/bin/phpunit
```