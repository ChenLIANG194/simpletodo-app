#!/bin/sh
composer install
npm install
npm run dev
php artisan serve --host=0.0.0.0 --port=8080
exec "$@"